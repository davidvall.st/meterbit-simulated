# Copyright (c) Microsoft. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for full license information.
import time
import uuid
import datetime
import csv
import os
# Using the Python Device SDK for IoT Hub:
#   https://github.com/Azure/azure-iot-sdk-python
# The sample connects to a device-specific MQTT endpoint on your IoT Hub.
from azure.iot.device import IoTHubDeviceClient, Message

# The device connection string to authenticate the device with your IoT hub.
# Using the Azure CLI:
# az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
CONNECTION_STRING = os.environ["IOT_DEVICE_CONNECTION_STRING"]

# Define the JSON message to send to IoT Hub.
MSG_TXT= {
    "mt": "mm",
    "tss": "",
    "tsu": "",
    "vl1": 24,
    "cl1": 24,
    "apl1": 24,
    "rpl1": 24,
    "frq": 24,
    "pfl1": 24,
    "aei": 24,
    "aee": 24,
    "rei": 24,
    "ree": 24,
    "apil1": 24,
    "rpil1": 24,
    "rpel1": 24,
    "apti": 99,
    "apte": 24,
    "rpti": 2,
    "rpte": 24,
    "appti": 24,
    "appti": 4
}

# MSG_TXT = ':[{"dev":"1cb9a46f-bc83-49f6-b410-508fac3dccf4","mt":"d","mod":"DTS27","ser":"2007700417","tz":"America/Bogota","tss":"2021-12-23T22:43:47+0000","tsl":"2021-12-23T17:43:47","ns":{"ls":0,"at":174,"tt":672}}'

def iothub_client_init():
    # Create an IoT Hub client
    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)
    return client


def iothub_client_telemetry_sample_run():

    
    client = iothub_client_init()
    cnt = 0
    print("IoT Hub device sending periodic messages, press Ctrl-C to exit")

    while True:
        # Build the message with simulated telemetry values.
        cnt = cnt+1
        dateStart = str(datetime.datetime.now())
        ts = datetime.datetime.now(datetime.timezone.utc)
        dateStart = str(ts.strftime("%Y-%m-%dT%H:%M:%S"))
        dateSender = str(ts.strftime("%Y-%m-%dT%H:%M:%S"))
        MSG_TXT["tsu"] = dateStart
        MSG_TXT["tss"] = dateSender
        try:
            # MSG_TXT = '{"mt":"m","ts":"'+dateStart+'","dev":"BYD003","cid":"EBF","mod":"byd_ebf","lic":"JTP149","lat":4.683127,"lon":-74.133164,"alt":2562.6,"acc":0,"cur":0,"vol":533.00,"soc":28,"sta":"OFF","spe":0,"odo":7046,"miv":3.335,"mav":3.338,"mit":21.00,"mat":25.00,"bra":false,"cst":"OFF","mot":35,"ds":false,"cv":[],"ct":[],"ene":0,"reg":0,"sbt":0,"hid":"XAgGxXMs6","sig":20,"gac":8,"dte":45,"fsd":98,"ext":{"idx":'+str(cnt)+'}}'
            message = Message(str(MSG_TXT).replace("'","\""))
            message.content_encoding = 'utf-8'
            message.content_type = 'application/json'
                # Add a custom application property to the message.
            # An IoT hub can filter on these properties without access to the message body.

            # Send the message.
            print("Sending message: {}".format(message))
            client.send_message(message)
            print("Message successfully sent")
            time.sleep(5)

        except KeyboardInterrupt:            
            print("IoTHubClient sample stopped")
            exit()


if __name__ == '__main__':
    print("IoT Hub Quickstart #1 - Simulated device")
    print("Press Ctrl-C to exit")
    iothub_client_telemetry_sample_run()
